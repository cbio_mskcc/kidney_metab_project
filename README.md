# Installation and Usage

    setRepositories(ind=1:6)
    options(repos="http://cran.rstudio.com/")
    if(!require(devtools)) { install.packages("devtools") }
    library(devtools)
    install_github("armish/metabologram")
    install_bitbucket("cbio_mskcc/kidney_metab_project", build_vignette=TRUE, dependencies=TRUE, args="--no-multiarch")

    library(kidneyMetabProject)
    runShinyApp()

# Application Structure

1. Code is pushed to https://bitbucket.org/cbio_mskcc/kidney_metab_project
2. Code pushes are listened by Docker Hub by this Docker container (https://hub.docker.com/r/cannin/kidney_metab_project/) triggering a automated rebuild of the Docker image described by the Dockerfile in the root directory of the BitBucket repository. The Docker image is all that is necessary for a working web application. The remaining steps automate reloading the application after a code commit on a web server.
3. Successful Docker image builds trigger a call to (http://sanderlab.org:3830/kidneyMetabProject) that can be simulated as the following in the command line:
   ``` 
   curl -X POST http://sanderlab.org:3830/kidneyMetabProject
   ```

4. This triggers the script kidneyMetabProject.sh under

    ```
    inst/script/dockerDeploy
    ```

    at

    ```
    https://bitbucket.org/cbio_mskcc/kidney_metab_project
    ```

    located at

    ```
    /webhooks on sanderlab.org
    ```
    
    to be triggered, which retrieves the newly built Docker image and restarts the web application. This step is carried out by using [captainhook](https://github.com/bketelsen/captainhook).