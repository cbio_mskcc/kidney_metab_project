!/bin/bash

echo "Pulling new version"
sudo docker pull cannin/kidney_metab_project

echo "Stopping new container"
sudo docker stop kidney_metab_project
sudo docker rm kidney_metab_project

echo "Starting new container"
sudo docker run --restart always --name kidney_metab_project -d -p 3839:3838 -t cannin/kidney_metab_project shiny-server
