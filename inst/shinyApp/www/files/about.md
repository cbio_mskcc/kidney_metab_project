### About the Data
Clear cell renal cell carcinoma (ccRCC) is the most common form of kidney cancer. We profiled 138 patients with varying stages of CCRCC for the abundance of ~800 metabolites. The data is provided here as a resource for the general research community.

### About Plotting
For visualization purposes, all metabolite abundances are log2-transformed in the plot window.

### About Metabolograms
Metabolograms are a visual tool for exploring metabolic pathways using both gene expression and metabolite abundance data. Data on the left corresponds to changes in gene expression, and data on the right correponds to changes in metabolite abundances. The center circle corresponds to the average value of a gene expression/metabolite abundance changes, and the outer circle shows the individual change for each constituent of the pathway.

In our study, changes in gene expression between clear-cell tumors and normal kidney samples are calculated from TCGA expression data using the limma voom package. Changes in metabolite abundance are calculated from the MSKCC metabolomics dataset using non-parametric U tests. All tests were corrected for multiple hypothesis testing using the Benjamini-Hochberg procedure. Fold changes for any metabolites/genes with associated p-value >0.05 were set to zero.

### About the Data Explorer
The data explorer was built by Augustin Luna, B. Arman Aksoy, and Ed Reznik using R Shiny.

### About the Research Team
Research conducted at Memorial Sloan Kettering Cancer Center.
