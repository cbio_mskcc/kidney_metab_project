library(shiny)
library(ggplot2)
library(markdown)
library(metabologram)
library(htmlwidgets)

# Load data
lapply(dir(file.path("www", "db"), pattern="RData", recursive=TRUE, full.names=TRUE), load, .GlobalEnv)
# If package installed then load data from package
require(kidneyMetabProject)

# aes_string in ggplotcannot accept names with spaces or colons
metabs <- paste0("metab", 1:nrow(normal))
names(metabs) <- rownames(normal)

shinyServer(function(input, output, session) {  
	output$plot <- renderPlot({
	    tmpClinVarTypes <- as.vector(clinvartypes)
	    names(tmpClinVarTypes) <- names(clinvartypes)
	    
	    tmpTumor <- t(tumor)
	    tmpNormal <- t(normal)
        
	    # Duplicate clinical data for matched normals
	    dupclin <- rbind(clin, clin)
        
        histogramFlag <- input$showHistogram
        type1 <- input$xAxisType
        id1 <- input$xAxis
        id2 <- input$yAxis
                
        if(type1 == "Metabolite") {
            name1 <- names(which(metabs == id1))         
        } else {
            name1 <- id1    
        }
        
	    name2 <- names(which(metabs == id2))   

	    cat("Histflag", histogramFlag, "\n")
	    cat("id1", id1, "\n")
	    cat("id2", id2, "\n")
	    cat("Name1", name1, "\n")
	    cat("Name2", name2, "\n")
	    cat("Type1", type1, "\n")
	    cat("tmpClinVarId", tmpClinVarTypes[id1], "\n")
	    cat("tmpClinVarName", tmpClinVarTypes[name1], "\n")

        p1 <- NULL

        if(type1 == "Metabolite") {
    	    if(histogramFlag) {
    	        cat("Plot1\n")
                
    	        plotData <- data.frame(x=tmpTumor[,name1])
                
    	        p1 <- ggplot(plotData, aes(x=x)) 
    	        p1 <- p1 + geom_bar() 
    	        p1 <- p1 + theme_classic() 
    	        p1 <- p1 + xlab(paste("Log2 Fold Change, Tumor:Normal,", name1)) 
    	        p1 <- p1 + ylab("Count")

    	    }

    	    if(!histogramFlag) {
    	        cat("Plot2\n")
    	 
    	        xData <- c(tmpTumor[,name1], tmpNormal[,name1])
    	        yData <- c(tmpTumor[,name2], tmpNormal[,name2])
                tissue <- c(rep("Tumor", nrow(tmpTumor)), rep("Normal", nrow(tmpNormal)))
                
                # Added to hide error of unequal column sizes
    	        validate(need(length(xData) == length(tissue), ""))
                
    	        plotData <- data.frame(x=xData, y=yData, Tissue=tissue)
    	        
              corval_tum = cor.test(plotData[which(tissue == 'Tumor'),1],
                                plotData[which(tissue == 'Tumor'),2],method = 'spearman')
              r_tum = as.character( signif(corval_tum$estimate,2) )
              p_tum = signif(corval_tum$p.value,2)
              if (p_tum<=1e-10){p_tum = '< 1e-10'}else{p_tum = as.character(p_tum)}
              
    	        corval_norm = cor.test(plotData[which(tissue == 'Normal'),1],
    	                              plotData[which(tissue == 'Normal'),2],method = 'spearman')
    	        r_norm = as.character( signif(corval_norm$estimate,2) )
    	        p_norm = signif(corval_norm$p.value,2)
    	        if (p_norm<=1e-10){p_norm = '< 1e-10'}else{p_norm = as.character(p_norm)}
              
    	        p1 <- ggplot(plotData, aes(x=x, y=y, color=Tissue)) 
    	        p1 <- p1 + geom_point(size=3) 
    	        p1 <- p1 + theme_classic() 
    	        p1 <- p1 + xlab(paste("Log2 Median Normalized Abundance,", name1)) 
    	        p1 <- p1 + ylab(paste("Log2 Median Normalized Abundance,", name2))
              p1 <- p1 + ggtitle(paste('Tumor: Spearman R ',r_tum, 'P-Value ',p_tum,'\nNormal: Spearman R ',r_norm, 'P-Value ',p_norm))
    	    }
        } 

        if(type1 == "Clinical" && !is.na(tmpClinVarTypes[name1])) {
            validate(need(!histogramFlag, "ERROR: Histograms are only available for metabolites."))
            
            if(!histogramFlag && tmpClinVarTypes[name1] == "Continuous") {
                cat("Plot3\n")
                
                xData <- clin[,name1]
                yData <- c(tmpTumor[,name2], tmpNormal[,name2])
                tissue <- c(rep("Tumor", nrow(tmpTumor)), rep("Normal", nrow(tmpNormal)))
                
                plotData <- data.frame(x=xData, y=yData, Tissue=tissue)
                
                p1 <- ggplot(plotData, aes(x=x, y=y, color=Tissue)) 
                p1 <- p1 + geom_point(size=3)
                p1 <- p1 + theme_classic()
                p1 <- p1 + xlab(name1)
                p1 <- p1 + ylab(paste("Log2 Median Normalized Abundance,", name2))
                p1 <- p1 + theme(axis.text.x = element_text(angle = 90, hjust = 1))
            }
            
            if(!histogramFlag && tmpClinVarTypes[name1] == "Discrete") {
                cat("Plot4\n")
                
                xData <- clin[,name1]
                yData <- c(tmpTumor[,name2], tmpNormal[,name2])
                tissue <- c(rep("Tumor", nrow(tmpTumor)), rep("Normal", nrow(tmpNormal)))
                
                plotData <- data.frame(x=xData, y=yData, Tissue=tissue)
                     
                p1 <- ggplot(plotData, aes(x=x, y=y, fill=Tissue))
                p1 <- p1 + geom_violin() 
                p1 <- p1 + geom_jitter(height=0)
                p1 <- p1 + theme_classic()
                p1 <- p1 + xlab(name1)
                p1 <- p1 + ylab(paste("Log2 Median Normalized Abundance,",name2))
                p1 <- p1 + facet_wrap(~Tissue)
                p1 <- p1 + theme(axis.text.x = element_text(angle = 90, hjust = 1))
            }
        }
	    
        print(p1)
	})
    
	output$xUi <- renderUI({
	    switch(input$xAxisType,
	           "Clinical" = selectInput("xAxis", "x-Axis Clinical Variable", choices=names(clinvartypes)),
	           #metab249 Glucose
	           "Metabolite" = selectInput("xAxis", "x-Axis Metabolite", choices=metabs, selected="metab249")
	    )
	})
    
	output$downloadNormalData <- downloadHandler(
	    filename="normalData.tsv",
	    content = function(file) {	        
	        write.table(normal, file, quote=FALSE, sep="\t", col.names=NA)
	    }
	)

    output$downloadTumorData <- downloadHandler(
        filename="tumorData.tsv",
        content = function(file) {	        
            write.table(tumor, file, quote=FALSE, sep="\t", col.names=NA)
        }
    )
    
	output$metabologram <- renderMetabologram({
      if (input$byvar == 'Tumor vs. Normal'){
        sampleMetabologramData <- allmgrams[[input$pathway]]
        legend2write <- "Log2 Ratio Tumor Abundance : Normal Abundance"
      }else{
        sampleMetabologramData <- allmgrams_stage[[input$pathway]]
        legend2write <- "Log2 Ratio AJCC Stage III/IV : AJCC Stage I/II"
      }
	    
        
	    sampleMetabologramBreaks <- c(
	        -3.00, -2.75, -2.50, -2.25, -2.00, -1.75, -1.50, -1.25, -1.00, 
	        -0.75, -0.50, -0.25, 0.00,  0.25,  0.50,  0.75,  1.00,  
	        1.25,  1.50,  1.75,  2.00,  2.25,  2.50,  2.75, 3.00
	    )
	    
	    sampleMetabologramColors <- c(
	        "#0000FF", "#1616FF", "#2C2CFF", "#4242FF", "#5858FF", "#6E6EFF", "#8585FF",
	        "#9B9BFF", "#B1B1FF", "#C7C7FF", "#DDDDFF", "#F3F3FF", "#FFF3F3", "#FFDDDD",
	        "#FFC7C7", "#FFB1B1", "#FF9B9B", "#FF8585", "#FF6E6E", "#FF5858", "#FF4242",
	        "#FF2C2C", "#FF1616", "#FF0000"
	    )
    
	    print(metabologram(
	        sampleMetabologramData, 
	        width=1200, 
	        height=1200, 
	        main=input$pathway,
	        showLegend=TRUE,
	        fontSize=10,
	        legendBreaks=sampleMetabologramBreaks,
	        legendColors=sampleMetabologramColors,
	        legendText=legend2write
	    ))
	})
})
